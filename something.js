var inputArray = [];
var individual;

class Individual {
	constructor(name, age, gender) {
		this.name = name;
		this.age = age;
		this.gender = gender;
		this.av = 3 * this.gender + this.age;
		this.color = "none";
	}
}

function submitForm() {
	getArray();
	var name = document.forms["form"]["name"].value;
	var age = Number(document.forms["form"]["age"].value);
	var selector = document.forms["form"]["gender"];
	var genderString = selector.options[selector.selectedIndex].value
	var genderNumber = getGenderNumber(genderString);
	
	var valid = validateForm(name, age, genderNumber);

	if(valid) {
		document.getElementById("myProgress").style.visibility = "visible";
		individual = new Individual(name, age, genderNumber);
		if(inputArray == null) {
			inputArray = [];
		}
		if(!checkIfAlreadyExists(individual)){
			inputArray.push(individual);	
		}
		window.localStorage.setItem("individual", JSON.stringify(individual));
		
		var sortedArray = inputArray.sort(sortFunction);
		let sum = sortedArray.reduce(function(sum, value) {
			return sum + Math.abs(individual.av - value.av);
		}, 0);
		let avg = sum / sortedArray.length;
		for (var i = sortedArray.length - 1; i >= 0; i--) {
			if(Math.abs(individual.av - sortedArray[i].av) > avg) {
				sortedArray[i].color = "red";
			} else {
				if(Math.abs(individual.av - sortedArray[i].av) >= avg/2) {
					sortedArray[i].color = "yellow";
				} else {
					sortedArray[i].color = "green";
				}
			}
		}
		
		window.localStorage.setItem("array", JSON.stringify(sortedArray));
		window.location.href = "results.html";
	}
	document.getElementById("myProgress").style.visibility = "hidden";
}

function sortFunction(a, b) {
	if(Math.abs(individual.av - a.av) > Math.abs(individual.av - b.av)) {
		return -1
	} else {
		return 1
	}
}

function getArray() {
	inputArray = JSON.parse(window.localStorage.getItem("array"));
}

function getIndividual() {
	individual = JSON.parse(window.localStorage.getItem("individual"));
}

function showIndividual() {
	getIndividual();
	document.write("<div>");
	document.write("<p>New Individual: </p>");
	document.write(individual.name + ", " + individual.age + ", " + getGenderString(individual.gender) + "<br>");
	document.write("AV: " + individual.av);
	document.write("</div>");
}

function showList() {
	getIndividual();
	getArray();

	document.write("<div>");
	document.write("<p>Organized List: </p>");
	for (var i = inputArray.length - 1; i >= 0; i--) {
		if(checkDiff(individual, inputArray[i])) {
			document.write("<div class=\"" + inputArray[i].color);
			document.write("\">");
			document.write(inputArray[i].name + ", " + inputArray[i].age + ", " + getGenderString(inputArray[i].gender) + "<br>");
			document.write("AV: " + inputArray[i].av);
			document.write("</div>")
		}
	}
	document.write("</ul>");
}

function checkIfAlreadyExists(individual) {
	for (var i = inputArray.length - 1; i >= 0; i--) {
		if(!checkDiff(individual, inputArray[i])) {
			return true;
		}
	}
	return false;
}

function checkDiff(one, two) {
	if(one.name != two.name || one.age != two.age || one.gender != two.gender || one.av != two.av) {
		return true;
	}
	return false;
}

function validateForm(name, age, gender) {
	if (name == "") {
		alert("Name must be filled out");
		return false;
	}
	if (age != null && age < 0) {
		alert("Age must be positive");
		return false;
	}
	return true;
}

function getGenderNumber(genderString) {
	if(genderString == "A") {
		return 5;
	} else if(genderString == "B") {
		return 2;
	} else if(genderString == "T") {
		return -1;
	}
}

function getGenderString(genderNumber) {
	if(genderNumber == 5) {
		return "A";
	} else if(genderNumber == 2) {
		return "B";
	} else if(genderNumber == -1) {
		return "T";
	}
}
